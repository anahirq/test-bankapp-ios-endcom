//
//  UIColor+Ext.swift
//  test-bankapp-ios-endcom
//
//  Created by Anahi Rojas on 28/09/21.
//

import UIKit

extension UIColor {
    static let darkBlue: UIColor = UIColor(named: "BADarkBlue")!
    static let mediumBlue: UIColor = UIColor(named: "BAMediumBlue")!
    static let green: UIColor = UIColor(named: "BAGreen")!
}

