//
//  ViewController.swift
//  test-bankapp-ios-endcom
//
//  Created by Anahi Rojas on 28/09/21.
//

import UIKit

class ViewController: UIViewController {
    
    let actionButton = BAButton(backgroundColor: UIColor.darkBlue, title: "Cancelar", fontColor: .white)
    let textField = BATextField(placeholder: "Nombre")
    let titleLabelBold = BABoldLabel(textAligment: .center, fontSize: 36, title: "Prueba", color: UIColor.darkBlue)


    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemPink
        layoutUI()
    }


    private func layoutUI() {
       
        view.addSubview(titleLabelBold)
        view.addSubview(actionButton)
        view.addSubview(textField)

        let padding: CGFloat = 20
        
        NSLayoutConstraint.activate([
            
            titleLabelBold.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: padding),
            titleLabelBold.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            titleLabelBold.widthAnchor.constraint(equalToConstant: 150),
            titleLabelBold.heightAnchor.constraint(equalToConstant: 38),
            
            actionButton.topAnchor.constraint(equalTo: titleLabelBold.bottomAnchor, constant: padding),
            
            textField.topAnchor.constraint(equalTo: actionButton.bottomAnchor, constant: padding)
            
            
            
            
           


            
        ])
    }
    
    
}

