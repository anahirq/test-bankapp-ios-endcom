//
//  BATextField.swift
//  test-bankapp-ios-endcom
//
//  Created by Anahi Rojas on 28/09/21.
//

import UIKit

class BATextField: UITextField {

    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    convenience init(placeholder: String){
        self.init(frame: .zero)
        self.placeholder = placeholder
    }
    
    private func configure(){
        translatesAutoresizingMaskIntoConstraints = false
        
        layer.cornerRadius = 2
        layer.borderWidth  = 1
        layer.borderColor  = UIColor.systemGray4.cgColor
        
        textColor                 = .label
        tintColor                 = .label
        textAlignment             = .left

        font?.withSize(9)
        adjustsFontSizeToFitWidth = true
        minimumFontSize           = 8
        
        backgroundColor    = .tertiarySystemBackground
        autocorrectionType = .no
        returnKeyType      = .go
       
        clearButtonMode    = .whileEditing

        
    }
}
