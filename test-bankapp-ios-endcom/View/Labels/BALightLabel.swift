//
//  BALightLabel.swift
//  test-bankapp-ios-endcom
//
//  Created by Anahi Rojas on 28/09/21.
//

import UIKit

class BALightLabel: UILabel {

    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    convenience init(fontSize: CGFloat, title: String) {
        self.init(frame: .zero)
        self.font = UIFont.systemFont(ofSize: fontSize, weight: .medium)
        self.text = title
    }
    
    
    private func configure() {
        textAlignment = .left
        textColor = UIColor.secondaryLabel
        adjustsFontSizeToFitWidth = true
        minimumScaleFactor = 0.90
        
        lineBreakMode = .byTruncatingTail
        translatesAutoresizingMaskIntoConstraints = false
    }

}
